import requests
import re
import os
from slugify import slugify
from bs4 import BeautifulSoup
import time

def progbar(curr, total, full_progbar):
    frac = curr/total
    filled_progbar = round(frac*full_progbar)
    print('\r', '#'*filled_progbar + '-'*(full_progbar-filled_progbar), '[{:>7.2%}]'.format(frac), end='')

def getPdfSlides(url):
    #print(url)
    #print(' Retrieving Slides url...')
    content = requests.get(url)
    contentText = content.text
    pattern = "http[^&]+"
    urls = re.findall(pattern, contentText)
    urlz = urls[0].split('&quot;')
    #print(urls)
    getSlidesPdfUrls = []
    for url in urls:
        #if url.find('slides.pdf') != -1:
        if 'slides.pdf' in url:
            if url not in getSlidesPdfUrls:
                getSlidesPdfUrls.append(url)
    #print('     '+str(len(getSlidesPdfUrls))+' slides found')
    return getSlidesPdfUrls

def getDatasets(payload):
    getDatasetsUrl = []
    for i in payload:
        if i.get('href').find('assets') != -1:
            getDatasetsUrl.append(i.get('href'))
    #print('     '+str(len(getDatasetsUrl))+' datasets found')
    return getDatasetsUrl

url = 'https://www.datacamp.com/tracks/data-scientist-with-python'
_1_level_url = 'https://www.datacamp.com/'
_1_level_links = []
_2_level_links = {}
_final_links = {}

print('Scanning '+url)
_1_level_r = requests.get(url)
html_doc = _1_level_r.content
soup = BeautifulSoup(html_doc,"html5lib")
links = soup.find_all('a', attrs = {"class": "course-block__footer-link link-unstyled"})

#_links_s = links[10:11]
_links_s = links
_firstCounter = 1;
progbar(0, len(_links_s), 50)
for i in _links_s:
    _1_level_url_target = _1_level_url + i.get('href')
    _1_level_links.append(_1_level_url_target)
    _2_level_r = requests.get(_1_level_url_target)
    html_doc_2 = _2_level_r.content
    soup_2 = BeautifulSoup(html_doc_2,"html5lib")
    title_2 = soup_2.find('h1', attrs = {"class": "header-hero__title"})
    #print('Scanning '+title_2.text+'...')
    description_2 = soup_2.find('p', attrs = {"class": "course__description"})
    links_2 = soup_2.find_all('a', attrs = {"class": "chapter__exercise-link"})
    chapters_title = [i.text.strip(' \t\n\r') for i in soup_2.find_all('h4',attrs={'class':'chapter__title'})]
    chapters_description = [i.text.strip('\n\r').strip() for i in soup_2.find_all('p',attrs={'class':'chapter__description'})]
    datasets = soup_2.find_all('a', attrs = {"class": "link-borderless"})
    slides = getPdfSlides(links_2[0].get('href'))
    _final_links[_1_level_url_target] = {'title':title_2.text,'description':description_2.text}
    _chapters_info = {}
    chapters_title = list(set(chapters_title))
    chapters_description = list(set(chapters_description))
    if len(slides)==0:
        slides = ['noslide'] * len(chapters_title)
    #print(slides)
    for idx, i in enumerate(chapters_title):
        #if idx == 0:
        _chapters_info['chapter'+str(idx+1)] = {'title':i,'description':chapters_description[idx],'slide':slides[idx]}
    _final_links[_1_level_url_target] ['chapters'] = _chapters_info
    _final_links[_1_level_url_target] ['datasets'] = getDatasets(datasets)
    progbar(_firstCounter, len(_links_s), 50)
    _firstCounter = _firstCounter+1
    #time.sleep(1)

print('\n')
#print(_final_links)
# Crate Root Folder
_rootFolder = 'Data_Scientist_with_Python_Track'
_absRootFolder = os.path.abspath('./'+_rootFolder+'/')
try:
    os.mkdir(_rootFolder)
except:
    pass


_courseCounter = 1;
for _course_key,_course in _final_links.items():
    print('Scanning ' + _course['title'])
    #create Course folder
    _courseFolderName = str(_courseCounter)+'_'+slugify(_course['title']).capitalize()
    _courseFolderPath = os.path.abspath(_absRootFolder+'/'+_courseFolderName+'/')
    os.makedirs(_courseFolderPath, exist_ok=True)

    ## create Course Readme file
    f = open(_courseFolderPath+"/readme.txt","w+")
    f.write(_course['description'])
    f.close()

    # Datasets
    _datasetsFolderPath = os.path.abspath(_courseFolderPath+'/Datasets/')
    os.makedirs(_datasetsFolderPath, exist_ok=True)

    # download datasets
    print('Downloading Datasets')
    progbar(0, len(_course['chapters']), 50)
    _progressBarCounter = 1
    for dataset in _course['datasets']:
        dataset_r = requests.get(dataset)
        dataset_split = dataset.split('/')
        with open(_datasetsFolderPath+'/'+dataset_split[-1], 'wb') as f:
            f.write(dataset_r.content)
            f.close
        progbar(_progressBarCounter, len(_course['datasets']), 50)
        _progressBarCounter = _progressBarCounter + 1
    print('\n')

    # Chapters
    print('Downloading Slides')
    progbar(0, len(_course['chapters']), 50)
    _chapterCounter = 1;
    _progressBarCounter = 1
    for _chapter_key,_chapter in _course['chapters'].items():
        #create Chapters folder
        _chapterFolderName = str(_chapterCounter)+'_'+slugify(_chapter['title']).capitalize()
        _chapterFolderPath = os.path.abspath(_courseFolderPath+'/'+_chapterFolderName+'/')
        #print('         Creating Chapter '+_chapter['title']+' folder...')
        os.makedirs(_chapterFolderPath, exist_ok=True)
        # creare chapter readme
        f = open(_chapterFolderPath+"/readme.txt","w+")
        f.write(_chapter['description'])
        f.close()
        if 'noslide' not in _chapter['slide']:
            slide_r = requests.get(_chapter['slide'], stream=True)
            slide_split = _chapter['slide'].split('/')
            slideName = slide_split[-1].split('&')
            handle = open(_chapterFolderPath+'/'+slideName[0], "wb")
            for chunk in slide_r.iter_content(chunk_size=512):
                if chunk:  # filter out keep-alive new chunks
                    handle.write(chunk)
        _chapterCounter = _chapterCounter + 1
        progbar(_progressBarCounter, len(_course['chapters']), 50)
        _progressBarCounter = _progressBarCounter + 1
    _courseCounter = _courseCounter + 1
    time.sleep(1)
    print('\n')


print(str(len(links))+ ' courses found')
print('\n')
print('Enjoy your Data Scientist with Python Track!')
